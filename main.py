from umqtt.simple import MQTTClient
from time import sleep
from machine import Pin, PWM, Timer, reset

from machine import Pin
from time import sleep
import dht


# **************************************#
# Global variables and constants:
mqtt_server = "zentrale"
topic_temp = b"pi/temperature/schlafzimmer"
topic_hum = b"pi/humidity/schlafzimmer"
topic_status = b"esp32/status"
topic_restart = b"esp32/reboot"
topic_get_values = b"esp32/values"

secondsSleep = 15
pin = 2


def sub_cb(topic, msg):
    if debug_mode:
        print((topic, msg))

    update_time()
    if debug_mode:
        print("topic {}: {:.0f}".format(topic_restart, topic == topic_restart))

    if topic == topic_restart:
        # restart system
        print("restart")
        client.publish(topic_status, "Restart")
        reset()

    elif topic == topic_get_values:
        if debug_mode:
            print('ESP received message: %s' % msg)

        print("get values")
        read_values()
    else:
        print(topic)


def update_time(comment=""):
    #print("update_time")
    client.publish(topic_status, comment + time_str(current_time()))


def connect_and_subscribe():
    cli = MQTTClient("esp32", mqtt_server)
    cli.set_callback(sub_cb)
    cli.connect()
    cli.subscribe(topic_status)
    if debug_mode:
        print('Connected to %s MQTT broker, subscribed to %s topic' % (mqtt_server, topic_status))
    cli.subscribe(topic_restart)
    if debug_mode:
        print('Connected to %s MQTT broker, subscribed to %s topic' % (mqtt_server, topic_restart))
    cli.subscribe(topic_get_values)
    if debug_mode:
        print('Connected to %s MQTT broker, subscribed to %s topic' % (mqtt_server, topic_get_values))
    cli.publish(topic_status, "Connected " + time_str(current_time()))
    return cli


def cb_alive(timer):
    update_time("Alive at ")


def restart_and_reconnect():
    if debug_mode:
        print('Failed to connect to MQTT broker. Reconnecting...')
    sleep(10)
    reset()


client = connect_and_subscribe()
update_time()

sensor = dht.DHT22(Pin(pin))
while True:
    try:
        sleep(secondsSleep)
        if not wlan.isconnected():
            if debug_mode:
                print('connecting to network...')
            # wlan.active(True)
            wlan.connect(wifi_credentials.ssid, wifi_credentials.password)
        measure_successful = False
        while not measure_successful:
            try:
                sensor.measure()
                measure_successful = True

                temp = sensor.temperature()
                client.publish(topic_temp, "{:.1f}".format(temp))

                hum = sensor.humidity()
                client.publish(topic_hum, "{:.1f}".format(hum))
                print('Temperature: {:.1f} °C, Humidity: {:.1f} %'.format(temp, hum))

            except OSError as e:
                print('Failed to read sensor.')

    except OSError as e:
        restart_and_reconnect()
