# See PyCharm help at https://www.jetbrains.com/help/pycharm/
import network
import wifi_credentials
import utime

import esp
import gc

from ntptime import settime

debug_mode = False
# debug_mode = True

esp.osdebug(None)
gc.collect()

# **************************************#
# Configure the ESP32 wifi as Station.
wlan = network.WLAN(network.STA_IF)
wlan.config(dhcp_hostname='esp32_temperature')

if not wlan.isconnected():
    if debug_mode:
        print('connecting to network...')
    # wlan.active(True)
    wlan.connect(wifi_credentials.ssid, wifi_credentials.password)
    while not wlan.isconnected():
        pass

if debug_mode:
    print('network config:', wlan.ifconfig())


hour_offset = 1


def current_time():
    return utime.localtime(utime.mktime(utime.localtime()) + hour_offset * 3600)


def time_str(time=current_time()):
    return ".".join("{:02d}".format(t) for t in reversed(time[0:3])) + " " +\
           ":".join("{:02d}".format(t) for t in time[3:6])


if utime.localtime()[0] < 2021:
    if debug_mode:
        print("initial time: {}".format(time_str()))
    settime()
    if debug_mode:
        print("time set: {}".format(time_str()))
elif debug_mode:
    print("current time: {}".format(time_str()))

